===== DEPLOY TRAEFIK ===== 

- 01 : automatic TLS / SSL, using ingressroute provider from traefik
- 02 : using certmanager and ingress
- 03 : same as 02, but only 1 issuer deployed, not in all namespace

> -- 01 -------------------------------------------------------------------------------------------

1. install traefik using helm and pass value from file
    - helm install traefik traefik/traefik -v 01-traefik.yaml
2. create ingress dev or prod
    - kubectl apply -f 01-ingress-dev.yaml
    - kubectl apply -f 01-ingress-prod.yaml

> -- 02 -------------------------------------------------------------------------------------------

1. install cert manager 1.10, please refer to latest version 
    - kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.10.1/cert-manager.yaml
2. install traefik using helm
    - helm install traefik traefik/traefik
3. create deployment
4. create service 
5. create issuer for dev and prod
    - kubectl apply -f 02-certmanager-issuer-dev.yaml
    - kubectl apply -f 02-certmanager-issuer-prod.yaml
        - certmanager-test.yaml for testing
6. create ingress for service ( dev or prod )
    - kubectl apply -f 02-ingress-dev-cm.yaml
    - kubectl apply -f 02-ingress-prod-cm.yaml

> -- 03 -------------------------------------------------------------------------------------------

1. install cert manager 1.10, please refer to latest version
    - kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.10.1/cert-manager.yaml
2. install traefik using helm
    - helm install traefik traefik/traefik
3. create deployment
4. create service 
5. create issuer for dev and prod
    - kubectl apply -f 03-cm-clusterissuer.yaml
        - certmanager-test.yaml for testing
6. create ingress for service ( dev or prod )
    - kubectl apply -f 03-ingress-dev-cm.yaml
    - kubectl apply -f 03-ingress-prod-cm.yaml


> -- TIPS & TRICK POST DEPLOYMENT -----------------------------------------------------------------------------------

1. app health in argocd always progressing
    - problem occurs if setup traefik using load balancer
    - to fix the problem, add this args in traefik deployment, or update traefik using helm and add additional value.yaml and add this
        - "--providers.kubernetesingress.ingressendpoint.publishedservice=traefik/traefik"
